dnl config.m4 for extension opengl

PHP_ARG_WITH(opengl, for OpenGL support,
[  --with-opengl[=DIR]       Include OpenGL support])

dnl CFLAGS="$CFLAGS -Wall -Wfatal-errors -shared -framework OpenGL"
CFLAGS="$CFLAGS -Wall -Wfatal-errors -shared -framework OpenGL"

if test "$PHP_OPENGL" != "no"; then

  if test -r $PHP_OPENGL/include/GL/gl.h; then
    INCLUDE_DIR=$PHP_OPENGL
  else
    AC_MSG_CHECKING(for OpenGL development files in default path)
    for i in /usr/X11R6 /usr/local /usr /System/Library/Frameworks/OpenGL.framework/Headers; do

      if test -r $i/include/GL/gl.h ; then
        INCLUDE_DIR=$i
        AC_MSG_RESULT(found in $i)
      fi

      if test -r $i/gl.h ; then
        INCLUDE_DIR=$i
        AC_MSG_RESULT(found in $i)
      fi

    done
  fi

  if test -z "$INCLUDE_DIR"; then
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR(Please make sure OpenGL is properly installed)
  fi

  AC_CHECK_PROG(SWIG_CHECK,swig,yes)
  if test x"$SWIG_CHECK" != x"yes" ; then
    AC_MSG_ERROR([Please install swig before installing.])
  fi

  AC_CONFIG_COMMANDS([swig], swig -php7 -I"$INCLUDE_DIR" opengl.i, [INCLUDE_DIR=$INCLUDE_DIR])

  PHP_ADD_INCLUDE($INCLUDE_DIR/include)
  PHP_ADD_INCLUDE($INCLUDE_DIR)

  PHP_SUBST(OPENGL_SHARED_LIBADD)
  PHP_NEW_EXTENSION(opengl, opengl_wrap.c, $ext_shared)
fi
