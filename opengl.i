%module opengl
%{
#include <OpenGL/gl.h>
#include <OpenGL/gl3.h>
%}
%include "gl.h"
%include "gl3.h"
